import React ,  { Component } from 'react';
import { StyleSheet, View , Text } from 'react-native';

class Lists extends Component {
    render(){
        return(
        <View>
          <Text> Hello !!!</Text>
        </View>
        );
    }
}

export default Lists;

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#000fff',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
  });